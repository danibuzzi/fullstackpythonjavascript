
from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Categoria(models.Model):
    descripcion = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.descripcion

class Producto(models.Model):

    titulo = models.CharField(max_length=45, null=False)
    imagen = models.FileField(upload_to='imagenes/')
    descripcion= models.CharField(max_length=250, null=False)
    categoria= models.ForeignKey(Categoria, on_delete=models.CASCADE,null=False, related_name="categoriaproducto")
    precio= models.FloatField(default=0.00, null=False) 
   
#   precio= models.FloatField(max_digits=10, decimal_places=2, default=0, null=False) 
    def __str__(self):
         return f"{self.titulo} - {self.descripcion} - {self.precio} ({self.categoria})"

class Carrito(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="usuario")
    productos= models.ManyToManyField(Producto)
    total= models.DecimalField(max_digits=10 , decimal_places=2, default=0) 

    def __str__(self):
        return f"{self.usuario} - {self.total}"





