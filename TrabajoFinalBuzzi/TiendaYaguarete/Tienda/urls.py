from django.urls import path
from .import views

urlpatterns = [
    path('', views.index, name="index"),
    path('acerca/', views.acerca, name="acerca"),
    path('filtro_categoria/<int:categoria_id>', views.filtro_categoria, name="filtro_categoria"),
    path('mostrar_producto/<int:producto_id>', views.mostrar_producto, name="mostrar_producto"),
    path('buscar_producto/<str:descripcion_prod>', views.buscar_producto, name="buscar_producto"),
    path('contacto/', views.contacto, name="contacto"),
    path('carrito/', views.carrito, name="carrito"),
    path('nuevo_producto/', views.nuevo_producto, name="nuevo_producto"),
    path('edicion_producto/<int:producto_id>', views.edicion_producto, name="edicion_producto"),
    path('eliminar_producto_bd/<int:producto_id>', views.eliminar_producto_bd, name="eliminar_producto_bd"),  
 
    
    # Para el carrito
    path("agregar/<int:producto_id>", views.agregar_producto, name="agregar"),
    path("eliminar/<int:producto_id>", views.eliminar_producto, name="eliminar"),
    path("restar/<int:producto_id>", views.restar_producto, name="restar"),
    path("limpiar/", views.limpiar_carro, name="limpiar"),


    #path('saludar/<str:nombre>', views.hola, name="hola"),
    #path('plantilla', views.plantilla, name="plantilla"),
    #path('plantillasaludo/<str:nombre>', views.saludar, name="saludar"),

]