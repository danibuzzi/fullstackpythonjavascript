# Generated by Django 3.2.4 on 2021-07-06 09:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Tienda', '0003_alter_carrito_total'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='precio',
            field=models.FloatField(default=0.0),
        ),
    ]
