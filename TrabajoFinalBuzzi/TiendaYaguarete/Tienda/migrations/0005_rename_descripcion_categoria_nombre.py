# Generated by Django 3.2.4 on 2021-07-07 08:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Tienda', '0004_alter_producto_precio'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categoria',
            old_name='descripcion',
            new_name='nombre',
        ),
    ]
