
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from django.utils.regex_helper import contains
from .forms import *
from .models import Carrito, Categoria,Producto
from django.http import HttpResponse
from .carro import Carro
from django.db.models import Q



# Create your views here.


def index(request):  

    try:
        registros=Producto.objects.count();
        registros_ultimos=registros-3;
        registros_anteriores=registros-10;
   
        return render(request,"tienda/index.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),
        #"primeros_productos" : Producto.objects.raw('SELECT * FROM Productos order by id DESC LIMIT 3'),
       "primeros_productos" : Producto.objects.all()[registros_ultimos:],
        "ultimos_productos": Producto.objects.all()[registros_anteriores:registros_ultimos],

        })
    except:
        return render(request,"tienda/index.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all()
        })

def acerca(request):
        return render(request,"tienda/acerca.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),
    })


def filtro_categoria(request,categoria_id):
    una_categoria = get_object_or_404(Categoria, id=categoria_id)
    queryset = Producto.objects.all()
    queryset = queryset.filter(categoria=una_categoria)
    #return HttpResponse(una_categoria.des)
    return render(request,"tienda/filtro_categoria.html",{"lista_categorias":Categoria.objects.all(), 
    "categoria_seleccionada":una_categoria, "lista_productos":queryset})



def buscar_producto(request,descripcion_prod):
    #items =  get_object_or_404(Producto, descripcion__contains=descripcion_prod)
    items = Producto.objects.filter(Q(descripcion__contains=descripcion_prod)|Q(titulo__contains=descripcion_prod))
    queryset = Producto.objects.all(),
    #return HttpResponse(una_categoria.des)
    return render(request,"tienda/buscar_producto.html",{"lista_categorias":Categoria.objects.all(), 
    "lista_productos":queryset, 
    "items_encontrados":items,"buscado":descripcion_prod})
 

   
def contacto(request):
        return render(request,"tienda/contacto.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),
    })

def carrito(request):
        return render(request,"tienda/carrito.html")

"""return render(request,"tienda/carrito.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),"""
    
    

def nuevo_producto(request):  
    if request.method == "POST":
        

        #cat = Categoria.objects.get(id=1)
        #form = FormProducto(request.POST, request.FILES, instance=Producto(imagen=request.FILES['imagen'], categoria=cat)) 
        form = FormProducto(request.POST, request.FILES, instance=Producto(imagen=request.FILES['imagen'])) 
        if form.is_valid():
            form.save()
        return redirect("/")    
        """return render(request,"tienda/index.html", {
        "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all()
        })"""
        #return render (request,"tienda/index.html") 
         #return render (request,"Tienda/index.html")          
    else:
        form = FormProducto()
    return render(request, "tienda/nuevo_producto.html", {
            "form": form, "lista_productos": Producto.objects.all(),
            "lista_categorias": Categoria.objects.all()
     })
def edicion_producto (request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    if request.method == "POST":  
        form = FormProducto(data=request.POST, files=request.FILES, instance=un_producto)
        if form.is_valid():
            form.save()
        return redirect("/")   
       
    else:
        form = FormProducto(instance = un_producto)
    return render(request, 'tienda/edicion_producto.html', {
            "un_producto": un_producto,
            "form": form
        })
  
""""def edicion_producto(request,producto_id):
   un_producto = get_object_or_404(Producto, id=producto_id)
   form = FormProducto(request.POST or None, request.FILES, instance=un_producto)  
   if form.is_valid():
        form.save()
        #messages.success(request,"SE edito correctamente") 
   return render(request,"tienda/edicion_producto.html", {
            "form":form ,"id":un_producto
   })
 
    
def articulo_editar(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        un_articulo.publicador = user
        form = FormArticulo(data=request.POST, files=request.FILES, instance=un_articulo)
        if form.is_valid():
            form.save()
            return redirect("sitio:index")
    else:
        form = FormArticulo(instance = un_articulo)
        return render(request, 'blog/articulo_edicion.html', {
            "articulo": un_articulo,
            "form": form
        })"""

def eliminar_producto_bd(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    un_producto.delete()
    #return render(request,"tienda/index.html")
    return redirect("index")
     

def mostrar_producto(request,producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    queryset = Producto.objects.all()
    una_categoria=un_producto.categoria
    #queryset = queryset.filter(id=un_producto)
    #return HttpResponse(una_categoria.des)
    return render(request,"tienda/mostrar_producto.html",{"lista_categorias":Categoria.objects.all(), 
    "producto_seleccionado":un_producto,"categoria_seleccionada":una_categoria, "lista_productos":queryset})        



# para el carrito
def agregar_producto(request, producto_id):
    
    carro=Carro(request)

    producto=Producto.objects.get(id=producto_id)

    carro.agregar(producto=producto)

    return redirect("/carrito")


def eliminar_producto(request, producto_id):

    carro=Carro(request)

    producto=Producto.objects.get(id=producto_id)

    carro.eliminar(producto=producto)

    return redirect("/carrito")


def restar_producto(request, producto_id):

    carro=Carro(request)

    producto=Producto.objects.get(id=producto_id)

    carro.restar_producto(producto=producto)

    return redirect("/carrito")


def limpiar_carro(request):

    carro=Carro(request)

    carro.limpiar_carro()

    return redirect("/carrito")


