def importe_total_carro(request):
    total=0
    if request.user.is_authenticated and  not request.user.is_superuser:
        try:
       
            for key,value in request.session["carro"].items():
                total=total+float(value["precio"])
        except:
             total=0
        
        
    return {"importe_total_carro":total}