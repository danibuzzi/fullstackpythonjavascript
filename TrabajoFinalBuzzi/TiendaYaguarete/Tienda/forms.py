from django import forms
from .models import Producto

class FormProducto(forms.ModelForm):

    #campos del modelo
    class Meta:
        model = Producto
        fields = ('titulo','imagen', 'descripcion','categoria','precio')
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'pub_titulo'}),
            'imagen': forms.FileInput(attrs={'name':'imagen_adjunta', 'class': 'pub_imagen'}),
            'categoria': forms.Select(attrs={'class': 'pub_categoria'}),
            'descripcion':forms.Textarea(attrs={'class': 'pub_categoria'}),
            'precio': forms.TextInput(attrs={'class': 'pub_precio'}),
           
        }
