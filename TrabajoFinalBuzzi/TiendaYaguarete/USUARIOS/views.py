from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from Tienda.models import Categoria,Producto

from .forms import *

def registrarse(request):
    
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            form.save()          
            return HttpResponseRedirect(reverse('index'),{ "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),})
    else:
        form = RegistroForm()
    return render(request, 'registration/registro.html', {
        'form': form,  "lista_productos": Producto.objects.all(),
        "lista_categorias": Categoria.objects.all(),
        })
